package gameEngine

const (
	Trail = iota
	Road
	Paved
)

var RoadNames = [...]string{"Trail", "Road", "Paved road"}

var RoadCoeff = [...]float32{0.9, 0.75, 0.5}

type TRoad struct {
	X           int
	Y           int
	Type        int
	TypeName    string
	TravelCoeff float32
}

var Roads = make([]*TRoad, 0)

func RoadInit() {
	var r TRoad
	for i := -MapSizeY / 2; i < MapSizeY/2; i++ {
		r = TRoad{
			X: -10, Y: i, Type: Road,
			TypeName: RoadNames[Road], TravelCoeff: RoadCoeff[Road],
		}
		Roads = append(Roads, &r)
		r = TRoad{
			X: 0, Y: i, Type: Road,
			TypeName: RoadNames[Road], TravelCoeff: RoadCoeff[Road],
		}
		Roads = append(Roads, &r)
		r = TRoad{
			X: 10, Y: i, Type: Road,
			TypeName: RoadNames[Road], TravelCoeff: RoadCoeff[Road],
		}
		Roads = append(Roads, &r)
	}
	for i := -MapSizeX / 2; i < MapSizeX/2; i++ {
		r = TRoad{
			X: i, Y: 0, Type: Paved,
			TypeName: RoadNames[Paved], TravelCoeff: RoadCoeff[Paved],
		}
		Roads = append(Roads, &r)
	}
}

func GetRoad(X, Y int) *TRoad {
	var t *TRoad
	for r := range Roads {
		if (Roads[r].X == X) && (Roads[r].Y == Y) &&
			((t == nil) || (RoadCoeff[Roads[r].Type] < RoadCoeff[t.Type])) {
			t = Roads[r]
		}
	}
	return t
}
