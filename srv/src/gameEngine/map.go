package gameEngine

import (
	"math/rand"
	"time"
)

const (
	Plains = iota
	Forest
	Mountain
	Sea
	Hills
	Desert
)

const TypesNum = Desert - Plains + 1

var TravelCost = [TypesNum]int{1, 2, 10, 30, 2, 3}

type Tile struct {
	X    int
	Y    int
	Id   int
	Type int
}

const MapSizeX int = 100
const MapSizeY int = 100

var Map [MapSizeX][MapSizeY]Tile

func MapInit() {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < MapSizeX; i++ {
		for j := 0; j < MapSizeY; j++ {
			Map[i][j] = Tile{
				X:    i - MapSizeX/2,
				Y:    j - MapSizeY/2,
				Id:   i*MapSizeY + j,
				Type: rand.Intn(TypesNum),
			}
		}
	}
}

func GetTile(x, y int) Tile {
	return Map[x+MapSizeX/2][y+MapSizeY/2]
}

func GetTileRect(x, y, dx, dy int) []Tile {
	var tiles = make([]Tile, 0)
	for i := 0; i < dx; i++ {
		tiles = append(tiles, Map[x+MapSizeX/2+i][y+MapSizeY/2:y+MapSizeY/2+dy]...)
	}
	return tiles
}
