package main

import (
	"gameEngine"
	"net/http"
	"network"
)

func main() {
	gameEngine.MapInit()
	gameEngine.RoadInit()
	http.HandleFunc("/ping", network.PingHandler.Handle)
	http.HandleFunc("/GetTiles", network.GetTilesHandler.Handle)
	http.HandleFunc("/Travel", network.TravelHandler.Handle)
	http.HandleFunc("/GetConfig", network.GetConfigHandler.Handle)
	http.HandleFunc("/GetState", network.GetStateHandler.Handle)
	http.HandleFunc("/GetObjects", network.GetObjectsHandler.Handle)
	http.ListenAndServe(":8080", nil)
}
