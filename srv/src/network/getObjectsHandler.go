package network

import (
	"gameEngine"
	"net/http"
	"player"
)

type getObjectsContent struct {
	Players []player.PlayerPublicData
	Roads   []gameEngine.TRoad
}

var GetObjectsHandler = requestHandler{
	NeedsAuthorization: true,
	HandleRequest:      getObjectsHandleRequest,
}

func getObjectsHandleRequest(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent) {
	e = ServerMsgError{Code: 0, Message: "Server status OK"}
	V := this.Player.Vision
	players := make([]player.PlayerPublicData, len(player.PlayerList))
	i := 0
	for _, v := range player.PlayerList {
		if (v.X >= this.Player.X-V) &&
			(v.X <= this.Player.X+V) &&
			(v.Y >= this.Player.Y-V) &&
			(v.X <= this.Player.Y+V) {
			players[i] = player.GetPublic(*v)
			i++
		}
	}
	roads := make([]gameEngine.TRoad, 0)
	var t *gameEngine.TRoad
	for x := this.Player.X - V; x <= this.Player.X+V; x++ {
		for y := this.Player.Y - V; y <= this.Player.Y+V; y++ {
			t = gameEngine.GetRoad(x, y)
			if t != nil {
				roads = append(roads, *t)
			}
		}
	}
	c = getObjectsContent{Players: players[:i], Roads: roads[:]}
	return e, c
}
