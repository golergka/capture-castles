package network

import (
	"encoding/json"
	"fmt"
	"net/http"
	"player"
)

type requestHandler struct {
	NeedsAuthorization bool
	HandleRequest      func(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent)
	Player             *player.PlayerData
}

var errorMsg = map[int]string{
	400: "Request needs authorization, but doesn't provide login",
	401: "Request login incorrect, can't authorize",
}

func returnRequestError(w http.ResponseWriter, code int) {
	w.WriteHeader(code)
	fmt.Printf("Client message error, code #%i, %s\n", code, errorMsg[code])
}

func (rh requestHandler) Handle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json, charset=utf-8")
	if rh.NeedsAuthorization {
		login := r.Header.Get("login")
		if login == "" {
			returnRequestError(w, 400)
			return
		}
		var err bool
		err, rh.Player = player.CheckAuthorization(login)
		if !err {
			returnRequestError(w, 401)
			return
		}
	}

	var e ServerMsgError
	var c ServerMsgContent
	var u ServerMsgUpdate
	e, c = rh.HandleRequest(&rh, r)
	if rh.NeedsAuthorization {
		u = formUpdate(rh.Player)
	} else {
		u = ServerMsgUpdate{}
	}
	response := &ServerMsgResponse{
		Error:   e,
		Content: c,
		Update:  u,
	}

	responseJson, err := json.Marshal(response)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	w.Write(responseJson)
}
