package network

import "player"

type ServerMsgUpdate struct {
	Profile player.PlayerData
	/* news, objects and so on */
}

func formUpdate(p *player.PlayerData) ServerMsgUpdate {
	return ServerMsgUpdate{Profile: *p}
}

type ServerMsgError struct {
	Code    int
	Message string
}

type ServerMsgContent interface{}

type ServerMsgResponse struct {
	Error   ServerMsgError
	Content ServerMsgContent
	Update  ServerMsgUpdate
}
