package network

import (
	"net/http"
)

type getConfigContent struct {
	MaxEnergy int
}

var GetConfigHandler = requestHandler{
	NeedsAuthorization: true,
	HandleRequest:      getConfigHandleRequest,
}

func getConfigHandleRequest(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent) {
	e = ServerMsgError{Code: 0, Message: "Server status OK"}
	c = getConfigContent{MaxEnergy: this.Player.MaxEnergy}
	return e, c
}
