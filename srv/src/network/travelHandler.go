package network

import (
	"gameEngine"
	"math"
	"net/http"
	"strconv"
)

var TravelHandler = requestHandler{
	NeedsAuthorization: true,
	HandleRequest:      travelHandleRequest,
}

func travelHandleRequest(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent) {

	x_str := r.Header.Get("target_x")
	if x_str == "" {
		e = ServerMsgError{Code: 1, Message: "Travel target_x not set"}
		return e, c
	}
	y_str := r.Header.Get("target_y")
	if y_str == "" {
		e = ServerMsgError{Code: 1, Message: "Travel target_y not set"}
		return e, c
	}

	x, err2 := strconv.Atoi(x_str)
	if err2 != nil {
		e = ServerMsgError{Code: 2, Message: "Travel target_x bad format (must be int)"}
		return e, c
	}
	y, err2 := strconv.Atoi(y_str)
	if err2 != nil {
		e = ServerMsgError{Code: 2, Message: "Travel target_y bad format (must be int)"}
		return e, c
	}

	if math.Abs(float64(x-this.Player.X))+math.Abs(float64(y-this.Player.Y)) > 1.00001 {
		e = ServerMsgError{Code: 2, Message: "Travel not allowed (too far)"}
		return e, c
	}

	cost := gameEngine.TravelCost[gameEngine.GetTile(x, y).Type]
	road := gameEngine.GetRoad(x, y)
	if road != nil {
		cost = int(float32(cost) * gameEngine.RoadCoeff[road.Type])
	}

	if this.Player.Energy < cost {
		e = ServerMsgError{Code: 2, Message: "Travel not allowed (not enough energy)"}
		return e, c
	}

	this.Player.X = x
	this.Player.Y = y
	this.Player.Energy -= cost

	e = ServerMsgError{Code: 0, Message: "Travel done successfully"}
	c = getTilesContent{Tiles: this.Player.GetVisionTiles()}
	return e, c
}
