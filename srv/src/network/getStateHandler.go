package network

import (
	"net/http"
)

var GetStateHandler = requestHandler{
	NeedsAuthorization: true,
	HandleRequest:      getStateHandleRequest,
}

func getStateHandleRequest(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent) {
	e = ServerMsgError{Code: 0, Message: "Server status OK"}
	c = this.Player
	return e, c
}
