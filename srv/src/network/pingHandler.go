package network

import "net/http"

type pingContent struct{}

var PingHandler = requestHandler{
	NeedsAuthorization: false,
	HandleRequest:      pingHandleRequest,
}

func pingHandleRequest(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent) {
	e = ServerMsgError{Code: 0, Message: "Server status OK"}
	return e, nil
}
