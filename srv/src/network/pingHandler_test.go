package network

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPingHandler_NormalRequest_StandardResponse(t *testing.T) {
	// Arrange
	req, err := http.NewRequest("GET", "http://example.com/", nil)
	if err != nil {
		t.Fatal("Couldn't create request")
		return
	}
	w := httptest.NewRecorder()
	// Act
	PingHandler.Handle(w, req)
	// Assert
	const expected = "{\"Error\":{\"Code\":0,\"Message\":\"Server status OK\"},\"Content\":null,\"Update\":{\"Energy\":0,\"X\":0,\"Y\":0}}"
	if w.Body.String() != expected {
		t.Errorf("Expected : %v, got : %v", expected, w.Body.String())
	}
}

func TestPingHandler_PostRequest_NoResponse(t *testing.T) {
	// Arrange
	req, err := http.NewRequest("POST", "http://example.com/", nil)
	if err != nil {
		t.Fatal("Couldn't create request")
		return
	}
	w := httptest.NewRecorder()
	// Act
	PingHandler.Handle(w, req)
	if w.Code != 402 {
		t.Errorf("Unexpected code: %v", w.Code)
	}
	if len(w.HeaderMap) != 0 {
		t.Errorf("Response contains headers")
	}
	body := w.Body.String()
	if body != "" {
		t.Errorf("Body not empty: %v", body)
	}
}
