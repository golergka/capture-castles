package network

import (
	"gameEngine"
	"net/http"
)

type getTilesContent struct {
	Tiles []gameEngine.Tile
}

var GetTilesHandler = requestHandler{
	NeedsAuthorization: true,
	HandleRequest:      getTilesHandleRequest,
}

func getTilesHandleRequest(this *requestHandler, r *http.Request) (e ServerMsgError, c ServerMsgContent) {
	e = ServerMsgError{Code: 0, Message: "Server status OK"}
	c = getTilesContent{Tiles: this.Player.GetVisionTiles()}
	return e, c
}
