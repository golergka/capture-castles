package player

import (
	"gameEngine"
)

type PlayerData struct {
	Id                      int    `json:"-"`
	AccountName, AvatarName string `json:"-"`
	Energy                  int
	MaxEnergy               int `json:"-"`
	X, Y                    int
	Vision                  int `json:"-"`
	/* userProfile, news, objects and so on */
}

type PlayerPublicData struct {
	AvatarName string
	X, Y       int
}

func GetPublic(p PlayerData) PlayerPublicData {
	return PlayerPublicData{AvatarName: p.AvatarName, X: p.X, Y: p.Y}
}

const MaxVision int = 10

var PlayerList map[string]*PlayerData = make(map[string]*PlayerData)

func authorize(login string) (p *PlayerData, err int) {
	if _, ok := PlayerList[login]; !ok {
		player := PlayerData{
			Id:          len(PlayerList),
			AccountName: login,
			AvatarName:  login,
			Energy:      100,
			MaxEnergy:   100,
			X:           0,
			Y:           0,
			Vision:      5,
		}
		PlayerList[login] = &player
	}
	return PlayerList[login], 0
}

func CheckAuthorization(login string) (bool, *PlayerData) {
	//	player PlayerData
	if _, ok := PlayerList[login]; !ok {
		_, err := authorize(login)
		if err != 0 {
			panic(err)
		}
	}
	return true, PlayerList[login]
}

func (p *PlayerData) GetVisionTiles() []gameEngine.Tile {
	v := p.Vision
	x := p.X
	y := p.Y
	return gameEngine.GetTileRect(x-v, y-v, 2*v+1, 2*v+1)
}
