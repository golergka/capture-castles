using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Model.Response;

namespace Backend
{

	public class ServerCommunicator : MonoBehaviour
	{
		[SerializeField]
		string mr_serverAddress;

		public string Login { get; set; }

		public void SendRequest<T>(
				string					_endPoint,
				Action<T>				_onSuccess,
				Action<Error>			_onFailure,
				Dictionary<string, string>	_params = null
			)
		{
			_params = _params ?? new Dictionary<string, string>();
			_params["login"] = Login;
			StartCoroutine(SendRequestCoroutine(_endPoint, _onSuccess, _onFailure, _params));
		}

		private IEnumerator SendRequestCoroutine<T>(
				string						_endPoint,
				Action<T>					_onSuccess,
				Action<Error>				_onFailure,
				Dictionary<string, string>	_params
			)
		{
			var request = new WWW(mr_serverAddress + _endPoint, null, _params);
			yield return request;

			if (!string.IsNullOrEmpty(request.error))
			{
				Debug.LogError("WWW error: " + request.error);
				_onFailure(new Error(1, request.error));
			}
			else
			{
				var response = JsonUtility.FromJson<Response<T>>(request.text);

				OnUpdate(response.Update);

				if (response.Error != null &&
					response.Error.Code != 0)
				{
					Debug.LogError("Server error: " + response.Error.Message);
					_onFailure(response.Error);
				}
				else
				{
					_onSuccess(response.Content);
				}
			}
		}

		public Action<Update> OnUpdate = delegate{};
	}

}
