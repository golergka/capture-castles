using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

using Model;

namespace World
{

	public class TileBuilder : MonoBehaviour
	{
		[SerializeField]
		GameObject mr_tilePrefab;

		[SerializeField]
		float mr_yRange;

		[SerializeField]
		float mr_yRotationRange;

		readonly private Dictionary<Tile, GameObject> m_TileGameObjects = new Dictionary<Tile, GameObject>();

		private TileCollection m_TileCollection;

		public void Init(TileCollection _tileCollection)
		{
			Assert.IsNull(m_TileCollection);
			Assert.IsNotNull(_tileCollection);

			m_TileCollection = _tileCollection;

			foreach(var tile in _tileCollection)
			{
				AddTile(tile);
			}

			m_TileCollection.OnAdd += AddTile;
		}

		public void Uninit()
		{
			Assert.IsNotNull(m_TileCollection);

			m_TileCollection.OnAdd -= AddTile;
			m_TileCollection = null;

			foreach(var t in m_TileGameObjects.Values)
			{
				Destroy(t);
			}
			m_TileGameObjects.Clear();
		}

		public GameObject TileGameObject(Tile _tile)
		{
			return m_TileGameObjects[_tile];
		}

		private void AddTile(Model.Tile _newTile)
		{
			var tileGO = Instantiate(mr_tilePrefab);
			tileGO.transform.parent = transform;
			tileGO.transform.localPosition = new Vector3(
					_newTile.X, 
					Random.Range(-mr_yRange, mr_yRange), 
					_newTile.Y
				);
			tileGO.transform.eulerAngles = new Vector3(
					0,
					Random.Range(-mr_yRotationRange, mr_yRotationRange),
					0
				);
			m_TileGameObjects[_newTile] = (tileGO);
		}
	}

}
