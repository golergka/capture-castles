using UnityEngine;
using UnityEngine.Assertions;

using Model;

namespace World
{

	public class PlayerPiece : MonoBehaviour
	{
		private Profile m_Profile;
		private ElevationProvider m_ElevationProvider;

		private void Awake()
		{
			gameObject.SetActive(false);
		}

		public void Init(
				Profile				_profile,
				ElevationProvider	_elevationProvider
			)
		{
			Assert.IsNull(m_ElevationProvider);
			Assert.IsNull(m_Profile);
			Assert.IsNotNull(_profile);

			gameObject.SetActive(true);

			m_Profile = _profile;
			m_Profile.OnUpdate += UpdateProfile;
			m_ElevationProvider = _elevationProvider;

			UpdateProfile();
		}

		public void Uninit()
		{
			Assert.IsNotNull(m_Profile);
			Assert.IsNotNull(m_ElevationProvider);

			gameObject.SetActive(false);

			m_Profile.OnUpdate -= UpdateProfile;
			m_Profile = null;
			m_ElevationProvider = null;
		}

		private void UpdateProfile()
		{
			transform.localPosition = new Vector3(
					m_Profile.X,
					m_ElevationProvider(m_Profile.X, m_Profile.Y),
					m_Profile.Y
				);
		}
	}

}
