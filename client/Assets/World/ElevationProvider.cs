namespace World
{
	/*
	 * Delegate that provides elevation at given coordinates
	 * in world space
	 */
	public delegate float ElevationProvider(int _x, int _y);
}
