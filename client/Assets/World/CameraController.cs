using UnityEngine;

namespace World
{
	public class CameraController : MonoBehaviour
	{
		[SerializeField] GameObject mr_Target;

		Vector3 m_PositionDelta;

		private void Start()
		{
			m_PositionDelta = mr_Target.transform.position
				- transform.position;
		}

		private void Update()
		{
			transform.position = mr_Target.transform.position
				- m_PositionDelta;
		}
	}
}
