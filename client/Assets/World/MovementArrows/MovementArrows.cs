using System;
using UnityEngine;
using UnityEngine.Assertions;

using Model;

namespace World
{
	public class MovementArrows : MonoBehaviour
	{
		[SerializeField] MovementArrow mr_NorthArrow;
		[SerializeField] MovementArrow mr_EastArrow;
		[SerializeField] MovementArrow mr_SouthArrow;
		[SerializeField] MovementArrow mr_WestArrow;

		private void Awake()
		{
			gameObject.SetActive(false);
		}

		private void Start()
		{
			mr_NorthArrow.OnArrowClicked += () => Travel(Direction.North);
			mr_EastArrow.OnArrowClicked += () => Travel(Direction.East);
			mr_SouthArrow.OnArrowClicked += () => Travel(Direction.South);
			mr_WestArrow.OnArrowClicked += () => Travel(Direction.West);
		}

		private void OnEnable()
		{
			mr_NorthArrow.enabled = true;
			mr_EastArrow.enabled = true;
			mr_SouthArrow.enabled = true;
			mr_WestArrow.enabled = true;
		}

		private void OnDisable()
		{
			mr_NorthArrow.enabled = false;
			mr_EastArrow.enabled = false;
			mr_SouthArrow.enabled = false;
			mr_WestArrow.enabled = false;
		}

		private void Travel(Direction _direction)
		{
			if (m_Travel != null)
			{
				m_Travel(_direction);
			}
		}

		private Profile				m_Profile;
		private ElevationProvider	m_ElevationProvider;
		private Action<Direction>	m_Travel;

		public void Init(
				Profile				_profile,
				ElevationProvider	_elevationProvider,
				Action<Direction>	_travel
			)
		{
			Assert.IsNull(m_Profile);
			Assert.IsNull(m_ElevationProvider);
			Assert.IsNull(m_Travel);

			gameObject.SetActive(true);

			m_Profile = _profile;
			m_Profile.OnUpdate += UpdatePosition;
			m_ElevationProvider = _elevationProvider;
			m_Travel = _travel;

			UpdatePosition();
		}

		public void Uninit()
		{
			Assert.IsNotNull(m_Profile);
			Assert.IsNotNull(m_ElevationProvider);
			Assert.IsNotNull(m_Travel);

			gameObject.SetActive(false);

			m_Profile.OnUpdate -= UpdatePosition;
			m_Profile = null;
			m_ElevationProvider = null;
			m_Travel = null;
		}

		private void UpdatePosition()
		{
			var x = m_Profile.X;
			var y = m_Profile.Y;
			transform.localPosition = new Vector3(x, 0, y);

			mr_NorthArrow.SetElevation(m_ElevationProvider(x, y + 1));
			mr_EastArrow.SetElevation(m_ElevationProvider(x + 1, y));
			mr_SouthArrow.SetElevation(m_ElevationProvider(x, y - 1));
			mr_WestArrow.SetElevation(m_ElevationProvider(x - 1, y));
		}
	}
}
