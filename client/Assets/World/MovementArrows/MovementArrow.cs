using System;
using UnityEngine;
using UnityEngine.EventSystems;


namespace World
{
	public class MovementArrow : MonoBehaviour, IPointerClickHandler
	{
		public Action OnArrowClicked = delegate {};
		
		public void OnPointerClick(PointerEventData _)
		{
			OnArrowClicked();
		}

		public void SetElevation(float _elevation)
		{
			var pos = transform.localPosition;
			pos.y = _elevation;
			transform.localPosition = pos;
		}
	}
}
