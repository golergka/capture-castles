using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

using Model;
using Model.Response;

public class Genesis : MonoBehaviour
{
	[SerializeField] World.TileBuilder mr_TileBuilder;
	[SerializeField] World.PlayerPiece mr_PlayerPiece;
	[SerializeField] World.MovementArrows mr_MovementArrows;

	[SerializeField] Backend.ServerCommunicator mr_ServerCommunicator;

	[SerializeField] UI.Login mr_LoginUI;
	[SerializeField] UI.Status mr_StatusUI;
	[SerializeField] UI.Message mr_MessageUI;

	const string k_PlayerPrefsUserName = "USER_NAME";

	private GameState m_State;

	void Start()
	{
		AskPlayerToLogin();
	}

	private void AskPlayerToLogin()
	{
		var savedUsername = PlayerPrefs.GetString(k_PlayerPrefsUserName);
		mr_LoginUI.AskUsername(savedUsername, Login);
	}

	private void Login(string _username)
	{
		Assert.IsNull(m_State);
		m_State = new GameState();
		mr_ServerCommunicator.OnUpdate += m_State.Update;

		mr_ServerCommunicator.Login = _username;

		mr_ServerCommunicator.SendRequest<GetTilesContent>(
			_endPoint: "/GetTiles",
			_onSuccess: delegate(GetTilesContent _content)
			{
				PlayerPrefs.SetString(k_PlayerPrefsUserName, _username);
				PlayerPrefs.Save();

				m_State.TileCollection.Update(_content.Tiles);

				// TileBuilder should be initialized before the player!
				mr_TileBuilder.Init(m_State.TileCollection);
				mr_PlayerPiece.Init(
						_profile: m_State.Profile,
						_elevationProvider: ElevationProvider
					);
				mr_MovementArrows.Init(
						_profile: m_State.Profile,
						_elevationProvider: ElevationProvider,
						_travel: Travel
					);
				mr_StatusUI.Init(m_State.Profile);
			},
			_onFailure: delegate(Error error)
			{
				mr_MessageUI.ShowMessage(error.Message, AskPlayerToLogin);
			}
		);
	}

	private float ElevationProvider(int _x, int _y)
	{
		var tile = m_State
			.TileCollection
			.TileAt(_x, _y);
		var tileGameObject = mr_TileBuilder.TileGameObject(tile);
		var tileCollider = tileGameObject.GetComponent<Collider>();
		return tileCollider.bounds.max.y;
	}

	private void Travel(World.Direction _direction)
	{
		int x, y;
		switch(_direction)
		{
			case(World.Direction.North):
				x = 0; y = 1;
				break;
			case(World.Direction.East):
				x = 1; y = 0;
				break;
			case(World.Direction.South):
				x = 0; y = -1;
				break;
			case(World.Direction.West):
				x = -1; y = 0;
				break;
			default:
				throw new System.InvalidOperationException("Unknown World.Direction value: " + _direction);
		}

		mr_MovementArrows.enabled = false;
		mr_ServerCommunicator.SendRequest<GetTilesContent>(
			_endPoint:	"/Travel",
			_onSuccess: delegate(GetTilesContent _content)
			{
				m_State.TileCollection.Update(_content.Tiles);
				mr_MovementArrows.enabled = true;
			},
			_onFailure: delegate(Error error)
			{
				mr_MessageUI.ShowMessage(error.Message, () => mr_MovementArrows.enabled = true );
			},
			_params:	new Dictionary<string, string>()
			{
				{ "target_x", (m_State.Profile.X + x).ToString() },
				{ "target_y", (m_State.Profile.Y + y).ToString() }
			}
		);
	}

	// Stub
	private void Logout()
	{
		Assert.IsNotNull(m_State);
		mr_ServerCommunicator.OnUpdate -= m_State.Update;

		mr_TileBuilder.Uninit();
		mr_PlayerPiece.Uninit();
		mr_StatusUI.Uninit();
	}
}
