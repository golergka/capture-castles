using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace UI
{

	public class Status : MonoBehaviour
	{
		[SerializeField] private Text mr_EnergyAmount;

		private Model.Profile m_Profile;

		public void Init(Model.Profile _profile)
		{
			Assert.IsNull(m_Profile);
			Assert.IsNotNull(_profile);

			gameObject.SetActive(true);

			m_Profile = _profile;
			UpdateStatus();
			m_Profile.OnUpdate += UpdateStatus;
		}

		private void UpdateStatus()
		{
			mr_EnergyAmount.text = m_Profile.Energy.ToString();
		}

		public void Uninit()
		{
			Assert.IsNotNull(m_Profile);

			m_Profile.OnUpdate -= UpdateStatus;
			m_Profile = null;
		}
	}

}
