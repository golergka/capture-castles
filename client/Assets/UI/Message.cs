using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace UI
{

	public class Message : MonoBehaviour
	{
		[SerializeField] private Text mr_MessageLabel;

		private Action m_OnComplete;

		public void ShowMessage(
				string _message,
				Action _onComplete
			)
		{
			Assert.IsNull(m_OnComplete);
			m_OnComplete = _onComplete;

			gameObject.SetActive(true);
			mr_MessageLabel.text = _message;
		}

		public void OnSubmit()
		{
			Assert.IsNotNull(m_OnComplete);
			gameObject.SetActive(false);
			m_OnComplete();
			m_OnComplete = null;
		}
	}

}
