using System;
using System.Linq;
using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace UI
{

	public class Login : MonoBehaviour
	{
		[SerializeField] private Text mr_ErrorLabel;
		[SerializeField] private InputField mr_LoginInput;
		[SerializeField] private float mr_ShakeAmount;
		[SerializeField] private float mr_ShakeTime;

		// Should be not null when and only when currently asking
		// for username
		private Action<string> m_OnSuccess;

		public void AskUsername(
				string			_savedUsername,
				Action<string>	_onSuccess
			)
		{
			Assert.IsNull(m_OnSuccess);

			m_OnSuccess = _onSuccess;
			gameObject.SetActive(true);
			mr_ErrorLabel.text = string.Empty;
			mr_LoginInput.text = _savedUsername;
		}

		public void OnSubmit()
		{
			Assert.IsNotNull(m_OnSuccess);

			var username = mr_LoginInput.text;

			if (CheckUsername(username))
			{
				m_OnSuccess(username);
				m_OnSuccess = null;
				gameObject.SetActive(false);
			}
			else
			{
				iTween.ShakePosition(
						gameObject,
						new Vector3(mr_ShakeAmount, 0, 0),
						mr_ShakeTime
					);
			}
		}

		public void OnUsernameChanged()
		{
			CheckUsername(mr_LoginInput.text);
		}

		private bool CheckUsername(string _username)
		{
			if (string.IsNullOrEmpty(_username))
			{
				mr_ErrorLabel.text = "Username can't be empty";
				return false;
			}
			if (_username.Any(c => Char.IsWhiteSpace(c)))
			{
				mr_ErrorLabel.text = "Username can't contain whitespace";
				return false;
			}
			mr_ErrorLabel.text = string.Empty;
			return true;
		}
	}

}
