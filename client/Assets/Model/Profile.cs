using System;

namespace Model
{

	public class Profile
	{
		public int X { get; private set; }
		public int Y { get; private set; }
		public int Energy { get; private set; }

		public Action OnUpdate = delegate{};

		public void Update(DTO.Profile _profileDTO)
		{
			X = _profileDTO.X;
			Y = _profileDTO.Y;
			Energy = _profileDTO.Energy;
			OnUpdate();
		}
	}
	
}
