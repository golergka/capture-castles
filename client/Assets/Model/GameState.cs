using Model.Response;

namespace Model
{

	public class GameState
	{
		readonly public TileCollection TileCollection = 
			new TileCollection(dto => new Tile(dto));

		readonly public Profile Profile = new Profile();

		public void Update(Update _update)
		{
			if (_update.Profile != null)
			{
				Profile.Update(_update.Profile);
			}
		}
	}

}
