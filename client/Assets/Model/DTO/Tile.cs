using System;

namespace Model.DTO
{

	[Serializable]
	public class Tile : IdBased
	{
		public int X;
		public int Y;
		public TileType Type;
	}

}
