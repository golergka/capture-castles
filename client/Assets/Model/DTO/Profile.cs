using System;

namespace Model.DTO
{

	[Serializable]
	public class Profile
	{
		public int Energy;
		public int X;
		public int Y;
	}

}
