using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

using Model.DTO;

namespace Model
{

	public class ModelCollection<TModel, TDTO> : IEnumerable<TModel>
		where TDTO : DTO.IdBased
		where TModel : Model<TDTO>
	{
		public ModelCollection(Func<TDTO, TModel> _modelFactory)
		{
			mr_ModelFactory = _modelFactory; 
		}

		private readonly Func<TDTO, TModel> mr_ModelFactory;
		private readonly Dictionary<int, Model<TDTO>> mr_Models = new Dictionary<int, Model<TDTO>>();

		public Action OnUpdate = delegate {};

		public Action<TModel> OnAdd = delegate {};
		
		public void Update(IEnumerable<TDTO> _DTOs)
		{
			Assert.IsNotNull(_DTOs);
			foreach(var dto in _DTOs)
			{
				Assert.IsNotNull(dto);
				if (mr_Models.ContainsKey(dto.Id))
				{
					mr_Models[dto.Id].Update(dto);
				}
				else
				{
					var newModel = mr_ModelFactory(dto);
					Assert.IsNotNull(newModel);
					mr_Models[dto.Id] = newModel;
					OnAdd(newModel);
				}
			}
			OnUpdate();
		}

		public IEnumerator<TModel> GetEnumerator()
		{
			return mr_Models.Values.Cast<TModel>().GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
	}

}
