using System;
using System.Linq;

using Model.DTO;

namespace Model
{
	public class TileCollection : ModelCollection<Tile, DTO.Tile>
	{
		public TileCollection(Func<DTO.Tile, Tile> _tileFactory)
			: base(_tileFactory)
		{ }

		public Tile TileAt(int _x, int _y)
		{
			return this.FirstOrDefault(t => t.X == _x && t.Y == _y);
		}
	}
}
