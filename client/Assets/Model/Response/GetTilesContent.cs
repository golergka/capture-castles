using System;
using System.Collections.Generic;
using Model.DTO;

namespace Model.Response
{

	[Serializable]
	public class GetTilesContent
	{
		public List<DTO.Tile> Tiles;
	}

}
