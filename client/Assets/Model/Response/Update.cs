using System;
using Model.DTO;

namespace Model.Response
{

	[Serializable]
	public class Update
	{
		public DTO.Profile Profile;
	}

}
