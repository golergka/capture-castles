using System;

namespace Model.Response
{

	[Serializable]
	public class Error
	{
		public Error(int _Code, string _Message)
		{
			Code = _Code;
			Message = _Message;
		}

		public int Code;
		public string Message;
	}

}
