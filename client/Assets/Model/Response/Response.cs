using System;

namespace Model.Response
{

	[Serializable]
	public class Response<T>
	{
		public Error Error;
		public T Content;
		public Update Update;
	}

}
