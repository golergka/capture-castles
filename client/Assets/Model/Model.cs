using UnityEngine.Assertions;
using Model.DTO;

namespace Model
{

	/*
	 * Object that can be created or updated on server response.
	 */
	public abstract class Model<T>
		where T : IdBased
	{
		public readonly int Id;

		public Model(T _DTO)
		{
			Id = _DTO.Id;
			Update(_DTO);
		}

		public abstract void Update(T _DTO);

		protected void AssertEqualTo(T _DTO)
		{
			Assert.AreEqual(Id, _DTO.Id, 
					"DTO_ID_ASSERT: Attempted to update tile with id " +
					Id + " from a DTO with id " + _DTO.Id);
		}
	}

}
