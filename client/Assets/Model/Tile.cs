using System;

namespace Model
{

	public class Tile : Model<DTO.Tile>
	{
		public Tile(DTO.Tile _DTO) : base(_DTO)
		{
			Update(_DTO);
		}

		public override void Update(DTO.Tile _DTO)
		{
			AssertEqualTo(_DTO);
			X = _DTO.X;
			Y = _DTO.Y;
			Type = _DTO.Type;
		}

		public int X { get; private set; }
		public int Y { get; private set; }
		public TileType Type { get; private set; }
	}

}
